
export const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "FOOD ORDERING APP",
            version: "1.0.0",
            contact: {
                name: "Ingale Sumit Rajendra",
                email: "sumit.r.ingale2001@gmail.com",
            },
            license: {
                name: "Apache 2.0",
                url: "http://apache.org/"
            }
        },
        servers: [
            {
                url: "http://localhost:8000/"
            }
        ],
        schemes: ['http', 'https'],
        paths: {

            // user endpoints start
            "/user/login": {
                post: {
                    summary: "Post user details",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Login", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/signup": {
                post: {
                    summary: "Post user details",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Sign up", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                        "name": { "type": "string" },
                                        "phone": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/changePassword": {
                put: {
                    summary: "Change user's password",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Change Password", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                        "confirmPassword": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/verifyUser": {
                get: {
                    summary: "Verify user",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/addToCart": {
                post: {
                    summary: "Add user's items to cart",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Add to cart", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "userID": { "type": "string" },
                                        "productID": { "type": "string" },
                                        "productName": { "type": "string" },
                                        "productImg": { "type": "string" },
                                        "quantity": { "type": "string" },
                                        "total": { "type": "string" },
                                        "restaurantID": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/cart/:id": {
                get: {
                    summary: "Get user cart",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "User ID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/deleteItem": {
                put: {
                    summary: "Delete user's cart items",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Delete Items", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "userID": { "type": "string" },
                                        "productID": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/buyProduct": {
                post: {
                    summary: "Buy product",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Buy", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "userID": { "type": "string" },
                                        "productDetails": {
                                            "type": "object", "properties": {
                                                "productID": { "type": "string" },
                                                "price": { "type": "string" },
                                                "quantity": { "type": "string" },
                                                "restaurantID": { "type": "string" },
                                            }
                                        },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/clearCart/:id": {
                put: {
                    summary: "Clear user cart",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "User ID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/pastOrders": {
                post: {
                    summary: "Post User's  orders details",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Buy", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "userID": { "type": "string" },
                                        "productDetails": {
                                            "type": "object",
                                            "properties": {
                                                "productID": { "type": "string" },
                                                "price": { "type": "string" },
                                                "quantity": { "type": "string" },
                                                "restaurantID": { "type": "string" },
                                            }
                                        },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/user/pastOrders/:id": {
                get: {
                    summary: "Get user past orders",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "User ID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            //user endpoints end 

            // restaurant endpoints start 
            "/restaurant/signup": {
                post: {
                    summary: "Post Restaurant details Singup",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Sign up", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                        "name": { "type": "string" },
                                        "address": { "type": "string" },
                                        "openingTime": { "type": "string" },
                                        "closingTime": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/login": {
                post: {
                    summary: "Post Restaurant details Login",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Login", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/changePassword": {
                put: {
                    summary: "Change restaurant's password",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Change Password", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email": { "type": "string" },
                                        "password": { "type": "string" },
                                        "confirmPassword": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/verifyRestaurant": {
                get: {
                    summary: "Verify Restaurant",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            // restaurant endpoints end 

            // restaurant products endpoint start 
            "/restaurant/products/addProduct/:restaurantID": {
                post: {
                    summary: "Add product",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "User ID",
                        required: true,
                        "type": "string"
                    }],

                    requestBody: {
                        "description": "Add product", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "object",
                                            "properties": {
                                                "price": { "type": "array" },
                                                "productName": { "type": "string" },
                                                "productImg": { "type": "string" },
                                                "desc": { "type": "string" },
                                                "category": { "type": "string" },
                                                "quantity": { "type": "string" },
                                            }
                                        }
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/all": {
                get: {
                    summary: "Get All Products",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },

            "/restaurant/products/singleRestaurant/:id": {
                get: {
                    summary: "Get single restaurant's products",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "Restaurant ID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/singleProduct/:id": {
                get: {
                    summary: "Get restaurant's single products",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "productID",
                        "in": "path",
                        "description": "productID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/delete": {
                put: {
                    summary: "Delete restaurant's product",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Delete product", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "productID": { "type": "string" },
                                        "restaurantID": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/update": {
                put: {
                    summary: "Update restaurant's product",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Update product", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "productID": { "type": "string" },
                                        "data": {
                                            "type": "object",
                                            "properties": {
                                                "productName": { "type": "string" },
                                                "productImg": { "type": "string" },
                                                "desc": { "type": "string" },
                                                "quantity": { "type": "string" },
                                                "price": { "type": "Array" },
                                                "category": { "type": "string" },
                                            }
                                        },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/orderedProducts/:id": {
                get: {
                    summary: "Get single restaurant's products",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    parameters: [{
                        "name": "ID",
                        "in": "path",
                        "description": "Restaurant ID",
                        required: true,
                        "type": "string"
                    }],
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            "/restaurant/products/status": {
                post: {
                    summary: "Change order status",
                    produces: ["application/json"],
                    consumes: ["application/json"],

                    requestBody: {
                        "description": "Status", "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "orderID": { "type": "string" },
                                        "productID": { "type": "string" },
                                    }
                                }
                            }
                        },
                    },
                    responses: {
                        500: {
                            description: "Internal server error",
                        },
                        200: {
                            description: "Success",
                        }
                    }
                }
            },
            // restaurant products endpoint end
        }
    },
    apis: ['./index.js']
}
