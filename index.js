import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import Connection from './database/db.js';
import bodyParser from 'body-parser';
import UserRoutes from './routes/userRoutes.js';
import RestaurantRoutes from './routes/restaurantRoutes.js'
import RestaurantProductRoutes from './routes/restProductRoutes.js'
import paymentRoutes from './routes/paymentRoutes.js'
import swaggerUI from 'swagger-ui-express'
import swaggerJSDoc from 'swagger-jsdoc';
import { options } from './swagger.js';
import fs from 'fs'

// port for the express server 
const PORT = 8000;

// initializing express
const app = express();

// middlewares 
app.use(express.json({ limit: "50mb" }));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true, limit: "50mb" }))
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, }))
app.use(cors({
    origin: "https://foodapp-capstone.vercel.app",
    credentials: true,
    methods: ['GET', 'PUT', 'POST', 'DELETE']
}))
app.use(cookieParser());



const swaggerSpec = swaggerJSDoc(options)

const customCss = fs.readFileSync((process.cwd() + "/swagger.css"), 'utf-8')
//swagger 
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpec, { customCss }))

// routing 
app.use("/user", UserRoutes);
app.use("/restaurant", RestaurantRoutes);
app.use("/restaurant/products", RestaurantProductRoutes)
app.use("/payment", paymentRoutes)


// database connection 
Connection()


// starting the server
app.listen(PORT, () => console.log(`server running on port ${PORT}`))


