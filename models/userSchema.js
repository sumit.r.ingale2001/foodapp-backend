import mongoose from "mongoose";


// creating a structure of the user collection 

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    profileImg: {
        type: String,
    },
    phone: {
        type: Number,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        default: "user"
    },
    cart: {
        type: Array,
        ref: "restaurant"
    },
    pastOrders:{type:Array, ref:"order"}
})

const User = mongoose.model("user", userSchema);
export default User;