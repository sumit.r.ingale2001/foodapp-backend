import jwt from 'jsonwebtoken';
import { SECRET_KEY } from '../passwordSecurity/pass.js';

export const userVerification = async (req, res, next) => {
    try {
        const token = req.cookies.access_token;
        if (!token) {
            return res.json({ message: "Token missing" })
        } else {
            jwt.verify(token, SECRET_KEY, (err, decoded) => {
                if (err) {
                    return res.json(err, "error with token");
                } else {
                    if (decoded.role === "user") {
                        next()
                    }
                    else {
                        return res.json({ message: "Not user" })
                    }
                }
            })
        }
    } catch (error) {
        res.json(error, "error while verifying")
    }
}

export const restaurantVerification = async (req, res, next) => {
    try {
        const token = req.cookies.access_token;
        if (!token) {
            return res.json({ message: "Token missing" })
        } else {
            jwt.verify(token, SECRET_KEY, (err, decoded) => {
                if (err) {
                    return res.json(err, "error with token");
                } else {
                    if (decoded.role === "admin") {
                        next()
                    }
                    else {
                        return res.json({ message: "Not admin" })
                    }
                }
            })
        }
    } catch (error) {
        res.json(error, "error while verifying")
    }
}